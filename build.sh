#! /bin/sh

set -e

docker_build() {
    dockerfile="$1"
    tag="$2"
    docker build ${CACHE_FROM} -t "${tag}" -f "${dockerfile}" .
}

docker_run() {
    tag="$1"
    command="$2"

    # Do we have a tty?
    it=""
    if [ -t 1 ] ; then
	it="-it"
    fi

    cache="$(pwd)/.docker_cache"

    mkdir -p ${cache}/go
    mkdir -p ${cache}/node_modules
    mkdir -p ${cache}/npm
    mkdir -p ./webapp/node_modules

    real_uid=$(id -u)
    real_gid=$(id -g)

    if [[ "${real_uid}" = "0" ]]; then
	image_home="/root"
    else
	image_home="/home/builder"
    fi

    volumes=""
    volumes="-v $(pwd):/src"
    
    volumes="${volumes} -v ${cache}/go:${image_home}/go"
    volumes="${volumes} -v ${cache}/npm:${image_home}/npm"
    volumes="${volumes} -v ${cache}/node_modules:/src/webapp/node_modules"

    docker run \
	   --rm ${it} \
	   ${volumes} \
	   -e REAL_UID=$(id -u) \
	   -e REAL_GID=$(id -g) \
	   -w /src \
	   "${tag}" "${command}"
}

docker_build build/Dockerfile crankykernel/maker:linux
docker_build build/Dockerfile.macos crankykernel/maker:macos

docker_run "crankykernel/maker:linux" "make install-deps"
docker_run "crankykernel/maker:linux" \
           "GOOS=linux GOARCH=amd64 make dist"
docker_run "crankykernel/maker:linux" \
           "CC=x86_64-w64-mingw32-gcc GOOS=windows GOARCH=amd64 make dist"

docker_run "crankykernel/maker:macos" "make install-deps"
docker_run "crankykernel/maker:macos" \
           "CC=o64-clang GOOS=darwin GOARCH=amd64 make dist"
