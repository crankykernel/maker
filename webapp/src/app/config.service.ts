// The MIT License (MIT)
//
// Copyright (c) 2018 Cranky Kernel
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy,
// modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
// BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
// ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

import {Injectable} from "@angular/core";
import {MakerApiService} from "./maker-api.service";
import {Observable, of} from "rxjs";
import {map, tap} from "rxjs/operators";
import {Logger, LoggerService} from "./logger.service";

export const DEFAULT_BALANCE_PERCENTS = "5,10,25,50,75,100";

export const DEFAULT_BINANCE_BNB_WARNING_LEVEL = 1.0;

@Injectable({
    providedIn: "root"
})
export class ConfigService {

    config: Config = null;

    private logger: Logger = null;

    constructor(private makerApi: MakerApiService,
                logger: LoggerService,
    ) {
        this.logger = logger.getLogger("config-server");
    }

    loadConfig(): Observable<any> {
        if (this.config) {
            this.logger.log("Returning cached configuration.");
            return of(this.config);
        }
        return this.makerApi.getConfig().pipe(map((config) => {
            this.logger.log("Fetching configuration from server.");
            this.config = config;
            if (this.config[ConfigKey.PREFERENCE_BINANCE_BNB_WARNING_LEVEL] === undefined) {
                this.config[ConfigKey.PREFERENCE_BINANCE_BNB_WARNING_LEVEL] = DEFAULT_BINANCE_BNB_WARNING_LEVEL;
            }
            return this.config;
        }));
    }

    getBalancePercents(): number[] {
        let percents = DEFAULT_BALANCE_PERCENTS;
        if (this.config[ConfigKey.PREFERENCE_BALANCE_PERCENTS]) {
            percents = this.config[ConfigKey.PREFERENCE_BALANCE_PERCENTS];
        }
        return percents.split(",").map((val) => {
            return +val;
        }).sort((a, b) => {
            return a - b;
        });
    }

    set(key: string, val: any) {
        this.config[key] = val;
    }

    saveBinanceConfig(): Observable<boolean> {
        return this.makerApi.post("/api/binance/config", {
            key: this.config[ConfigKey.BINANCE_API_KEY],
            secret: this.config[ConfigKey.BINANCE_API_SECRET],
        }).pipe(map(() => {
            return true;
        }));
    }

    savePreferences(): Observable<boolean> {
        return this.makerApi.savePreferences(this.config);
    }
}

export interface Config {
    [key: string]: any;
}

export enum ConfigKey {
    "BINANCE_API_KEY" = "binance.api.key",
    "BINANCE_API_SECRET" = "binance.api.secret",
    "PREFERENCE_BALANCE_PERCENTS" = "preferences.balance.percents",
    "PREFERENCE_BINANCE_BNB_WARNING_LEVEL" = "preferences.binance.bnbwarninglevel",
}
