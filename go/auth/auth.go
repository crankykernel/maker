// The MIT License (MIT)
//
// Copyright (c) 2019 Cranky Kernel
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy,
// modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
// BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
// ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

package auth

import (
	"bytes"
	"encoding/hex"
	"fmt"
	"golang.org/x/crypto/argon2"
	"math/rand"
	"strings"
)

const (
	TYPE_ARGON2ID = "argon2id"
)

const PASSWORD_TYPE = TYPE_ARGON2ID

const SALT_SIZE = 16

func genSalt() ([]byte, error) {
	return genRandom(SALT_SIZE)
}

func genRandom(size int) ([]byte, error) {
	bytes := make([]byte, size)
	_, err := rand.Read(bytes)
	return bytes, err
}

func encode(input []byte) string {
	return hex.EncodeToString(input)
}

func decode(input string) ([]byte, error) {
	return hex.DecodeString(input)
}

func CheckPassword(password string, encodedPassword string) (bool, error) {
	passwordType, salt, passwordHash, err := DecodePassword(encodedPassword)
	if err != nil {
		return false, err
	}
	switch passwordType {
	case TYPE_ARGON2ID:
		hash := argon2.IDKey([]byte(password), salt, 1, 64*1024, 4, 32)
		if bytes.Equal(hash, passwordHash) {
			return true, nil
		}
	}
	return false, nil
}

func DecodePassword(input string) (passwordType string, salt []byte, password []byte, err error) {
	parts := strings.Split(input, "$")
	if len(parts) != 3 {
		err = fmt.Errorf("invalid encoded password")
	} else {
		passwordType = parts[0]
		salt, err = decode(parts[1])
		password, err = decode(parts[2])
	}
	return passwordType, salt, password, err
}

func EncodePassword(password string) (string, error) {
	salt, err := genSalt()
	if err != nil {
		return "", err
	}
	encoded := argon2.IDKey([]byte(password), salt, 1, 64*1024, 4, 32)
	return fmt.Sprintf("%s$%s$%s",
		TYPE_ARGON2ID, encode(salt), encode(encoded)), nil
}
