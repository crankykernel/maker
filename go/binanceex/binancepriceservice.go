// The MIT License (MIT)
//
// Copyright (c) 2018 Cranky Kernel
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy,
// modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
// BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
// ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

package binanceex

import (
	"fmt"
	"github.com/crankykernel/binanceapi-go"
	"gitlab.com/crankykernel/maker/go/log"
	"gitlab.com/crankykernel/maker/go/types"
	"gitlab.com/crankykernel/maker/go/util"
)

type BinancePriceService struct {
	exchangeInfoService *ExchangeInfoService
	client              *binanceapi.RestClient
}

func NewBinancePriceService(exchangeInfoService *ExchangeInfoService) *BinancePriceService {
	return &BinancePriceService{
		exchangeInfoService: exchangeInfoService,
		client:              binanceapi.NewRestClient(),
	}
}

// GetLastPrice gets the most current close price from Binance using the REST
// API.
func (s *BinancePriceService) GetLastPrice(symbol string) (float64, error) {
	ticker, err := s.client.GetPriceTicker(symbol)
	if err != nil {
		return 0, err
	}
	return ticker.Price, nil
}

// GetBestBidPrice gets the most current best bid price from Binance using
// the REST API.
func (s *BinancePriceService) GetBestBidPrice(symbol string) (float64, error) {
	ticker, err := s.client.GetBookTicker(symbol)
	if err != nil {
		return 0, err
	}
	return ticker.BidPrice, nil
}

// GetBestBidPrice gets the most current best bid price from Binance using
// the REST API.
func (s *BinancePriceService) GetBestAskPrice(symbol string) (float64, error) {
	ticker, err := s.client.GetBookTicker(symbol)
	if err != nil {
		return 0, err
	}
	return ticker.AskPrice, nil
}

func (s *BinancePriceService) AdjustPriceByTicks(symbol string, price float64, ticks int64) float64 {
	tickSize, err := s.exchangeInfoService.GetTickSize(symbol)
	if err != nil {
		log.WithError(err).WithFields(log.Fields{
			"symbol": symbol,
		}).Errorf("Failed to lookup tick size")
	}
	return util.Round8(price + (tickSize * float64(ticks)))
}

func (s *BinancePriceService) GetPrice(symbol string, priceSource types.PriceSource) (float64, error) {
	switch priceSource {
	case types.PriceSourceLast:
		return s.GetLastPrice(symbol)
	case types.PriceSourceBestBid:
		return s.GetBestBidPrice(symbol)
	case types.PriceSourceBestAsk:
		return s.GetBestAskPrice(symbol)
	default:
		return 0, fmt.Errorf("unknown price source: %s", priceSource)
	}
}
