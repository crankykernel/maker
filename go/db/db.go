// The MIT License (MIT)
//
// Copyright (c) 2018 Cranky Kernel
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy,
// modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
// BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
// ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

package db

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"gitlab.com/crankykernel/maker/go/log"
	"gitlab.com/crankykernel/maker/go/types"
	"path"
	"strings"
	"time"
)

var db *sql.DB

func incrementVersion(tx *sql.Tx, version int) error {
	_, err := tx.Exec("insert into schema values (?, 'now')", version)
	return err
}

func initDb(db *sql.DB) error {
	var version = 0
	tx, err := db.Begin()
	if err != nil {
		return fmt.Errorf("failed to begin transaction: %v", err)
	}
	row := tx.QueryRow("select max(version) from schema")
	if err := row.Scan(&version); err != nil {
		log.Printf("Initializing database.")
		_, err := db.Exec("create table schema (version integer not null primary key, timestamp timestamp)")
		if err != nil {
			tx.Rollback()
			return fmt.Errorf("failed to create schema table: %v", err)
		}
		if err := incrementVersion(tx, 0); err != nil {
			tx.Rollback()
			return fmt.Errorf("failed to insert into schema table: %v", err)
		}
		version = 0
	} else {
		log.Printf("Found database version %d.", version)
	}

	if version < 1 {
		_, err := tx.Exec(`create table binance_raw_execution_report (timestamp timestamp, report json);`)
		if err != nil {
			tx.Rollback()
			return fmt.Errorf("failed to create binance_raw_execution_reports table: %v", err)
		}
		_, err = tx.Exec(`create index binance_raw_execution_report_timestamp_index on binance_raw_execution_report(timestamp)`)
		if err != nil {
			tx.Rollback()
			return fmt.Errorf("failed to create binance_raw_execution_reports_timestamp_index: %v", err)
		}
		if err := incrementVersion(tx, 1); err != nil {
			tx.Rollback()
			return err
		}
	}

	if version < 2 {
		_, err := tx.Exec(`create table binance_trade (id string primary key unique, archived bool default false, data json)`)
		if err != nil {
			tx.Rollback()
			return fmt.Errorf("failed to create binance_trade table: %v", err)
		}
		if err := incrementVersion(tx, 2); err != nil {
			tx.Rollback()
			return err
		}
	}

	if version < 3 {
		rows, err := tx.Query(`select id, data from binance_trade`)
		if err != nil {
			tx.Rollback()
			return fmt.Errorf("failed to load trades: %v", err)
		}

		count := 0
		for rows.Next() {
			var localId string
			var data string
			if err := rows.Scan(&localId, &data); err != nil {
				log.WithError(err).Error("Failed to scan row.")
				continue
			}

			var tradeState0 types.TradeStateV0
			if err := json.Unmarshal([]byte(data), &tradeState0); err != nil {
				log.WithError(err).Error("Failed to unmarshal v0 trade state.")
				continue
			}

			if tradeState0.Version > 0 {
				continue
			}

			tradeState := types.TradeStateV0ToTradeStateV1(tradeState0)
			TxDbUpdateTradeState(tx, &tradeState)
			count += 1
		}
		log.Printf("Migrated %d trades from v0 to v1.", count)
		if err := incrementVersion(tx, 3); err != nil {
			tx.Rollback()
			return err
		}
	}

	tx.Commit()
	return nil
}

func DbOpen(dataDirectory string) {
	var err error
	filename := path.Join(dataDirectory, "maker.db")
	log.Infof("Opening database %s", filename)
	db, err = sql.Open("sqlite3", filename)
	if err != nil {
		log.Fatal(err)
	}
	if err := initDb(db); err != nil {
		log.Fatal(err)
	}
}

func DbSaveBinanceRawExecutionReport(timestamp time.Time, event []byte) error {
	tx, err := db.Begin()
	if err != nil {
		return err
	}
	_, err = tx.Exec(`insert into binance_raw_execution_report (timestamp, report) values (?, ?)`,
		formatTimestamp(timestamp), event)
	if err != nil {
		tx.Rollback()
		return err
	}
	tx.Commit()
	return nil
}

func DbSaveTrade(trade *types.Trade) error {
	tx, err := db.Begin()
	if err != nil {
		return err
	}
	data, err := formatJson(trade.State)
	if err != nil {
		return err
	}
	_, err = tx.Exec(`insert into binance_trade (id, data) values (?, ?)`,
		trade.State.TradeID, data)
	tx.Commit()
	return err
}

func TxDbUpdateTradeState(tx *sql.Tx, trade *types.TradeState) error {
	data, err := formatJson(trade)
	if err != nil {
		return err
	}
	_, err = tx.Exec(`update binance_trade set data = ? where id = ?`,
		data, trade.TradeID)
	return err
}

func DbUpdateTrade(trade *types.Trade) error {
	tx, err := db.Begin()
	if err != nil {
		return err
	}
	err = TxDbUpdateTradeState(tx, &trade.State)
	if err != nil {
		log.WithError(err).Error("Failed to update trade to DB.")
		tx.Rollback()
	} else {
		tx.Commit()
	}
	return err
}

func DbArchiveTrade(trade *types.Trade) error {
	tx, err := db.Begin()
	if err != nil {
		return err
	}
	_, err = tx.Exec(`update binance_trade set archived = 1 where id = ?`,
		trade.State.TradeID)
	if err != nil {
		tx.Rollback()
		return err
	}
	tx.Commit()
	return err
}

func DbRestoreTradeState() ([]types.TradeState, error) {
	rows, err := db.Query(`select id, data from binance_trade where archived = 0`)
	if err != nil {
		return nil, err
	}

	tradeStates := []types.TradeState{}

	for rows.Next() {
		var localId string
		var data string
		if err := rows.Scan(&localId, &data); err != nil {
			return nil, err
		}
		var tradeState types.TradeState
		if err := json.Unmarshal([]byte(data), &tradeState); err != nil {
			return nil, err
		}

		tradeStates = append(tradeStates, tradeState)
	}
	return tradeStates, nil
}

func formatTimestamp(timestamp time.Time) string {
	return timestamp.UTC().Format("2006-01-02 15:04:05.999")
}

func formatJson(val interface{}) (string, error) {
	buf, err := json.Marshal(val)
	if err != nil {
		return "", err
	}
	return string(buf), nil
}

func DbGetTradeByID(tradeId string) (*types.TradeState, error) {
	row := db.QueryRow(
		`select data from binance_trade where id = ?`,
		tradeId)
	var data string
	err := row.Scan(&data)
	if err != nil {
		return nil, err
	}
	var tradeState types.TradeState
	err = json.Unmarshal([]byte(data), &tradeState)
	if err != nil {
		return nil, err
	}
	return &tradeState, nil
}

type TradeQueryOptions struct {
	IsClosed bool
	Offset   int64
	Limit    int64
}

func DbQueryTrades(options TradeQueryOptions) ([]types.TradeState, error) {

	where := []string{}

	if options.IsClosed {
		where = append(where, fmt.Sprintf("json_extract(binance_trade.data, '$.CloseTime') != ''"))
	}

	sql := "select id, data from binance_trade"
	if len(where) > 0 {
		sql = fmt.Sprintf("%s WHERE %s", sql, strings.Join(where, "AND "))
	}

	sql = fmt.Sprintf("%s ORDER BY json_extract(binance_trade.data, '$.CloseTime') DESC ", sql)
	sql = fmt.Sprintf("%s LIMIT %d OFFSET %d ", sql, options.Limit, options.Offset)

	log.Printf("%s", sql)

	rows, err := db.Query(sql)
	if err != nil {
		return nil, err
	}

	tradeStates := []types.TradeState{}

	for rows.Next() {
		var localId string
		var data string
		if err := rows.Scan(&localId, &data); err != nil {
			return nil, err
		}
		var tradeState types.TradeState
		if err := json.Unmarshal([]byte(data), &tradeState); err != nil {
			return nil, err
		}
		tradeStates = append(tradeStates, tradeState)
	}
	return tradeStates, nil
}
