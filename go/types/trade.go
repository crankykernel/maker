// The MIT License (MIT)
//
// Copyright (c) 2018 Cranky Kernel
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy,
// modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
// BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
// ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

package types

import (
	"github.com/crankykernel/binanceapi-go"
	"math"
	"time"
)

const TRADE_STATE_VERSION = 1

const DEFAULT_FEE = float64(0.001)
const BNB_FEE = float64(0.00075)

type Trade struct {
	State TradeState
}

func NewTrade() *Trade {
	return &Trade{
		State: TradeState{
			Version:        TRADE_STATE_VERSION,
			Status:         TradeStatusNew,
			Fee:            DEFAULT_FEE,
			OpenTime:       time.Now(),
			ClientOrderIDs: make(map[string]bool),
		},
	}
}

func NewTradeWithState(state TradeState) *Trade {
	if state.ClientOrderIDs == nil {
		state.ClientOrderIDs = make(map[string]bool)
	}
	trade := &Trade{
		State: state,
	}
	trade.UpdateSellState()
	trade.UpdateBuyState()
	return trade
}

func (t *Trade) IsDone() bool {
	switch t.State.Status {
	case TradeStatusDone:
	case TradeStatusCanceled:
	case TradeStatusFailed:
	case TradeStatusAbandoned:
	default:
		return false
	}
	return true
}

func (s *Trade) AddHistory(history HistoryEntry) {
	s.State.History = append(s.State.History, history)
}

func (s *Trade) AddHistoryEntry(historyType HistoryType, fields interface{}) {
	entry := HistoryEntry{
		Timestamp: time.Now(),
		Type:      historyType,
		Fields:    fields,
	}
	s.State.History = append(s.State.History, entry)
}

func (s *Trade) FeeAsset() string {
	lastFillIndex := len(s.State.BuySideFills) - 1
	if lastFillIndex < 0 {
		return ""
	}
	return s.State.BuySideFills[lastFillIndex].CommissionAsset
}

func (t *Trade) SetLimitSellByPercent(percent float64) {
	t.State.LimitSell.Enabled = true
	t.State.LimitSell.Type = LimitSellTypePercent
	t.State.LimitSell.Percent = percent
}

func (t *Trade) SetLimitSellByPrice(price float64) {
	t.State.LimitSell.Enabled = true
	t.State.LimitSell.Type = LimitSellTypePrice
	t.State.LimitSell.Price = price
}

func (t *Trade) SetStopLoss(enable bool, percent float64) {
	t.State.StopLoss.Enabled = enable
	t.State.StopLoss.Percent = percent
}

func (t *Trade) SetTrailingProfit(enable bool, percent float64, deviation float64) {
	t.State.TrailingProfit.Enabled = enable
	t.State.TrailingProfit.Percent = percent
	t.State.TrailingProfit.Deviation = deviation
}

func (t *Trade) AddBuyFill(report binanceapi.StreamExecutionReport) {
	fill := OrderFill{
		Price:            report.LastExecutedPrice,
		Quantity:         report.LastExecutedQuantity,
		CommissionAmount: report.CommissionAmount,
		CommissionAsset:  report.CommissionAsset,
	}
	t.DoAddBuyFill(fill)
}

func (t *Trade) DoAddBuyFill(fill OrderFill) {
	t.State.BuySideFills = append(t.State.BuySideFills, fill)
	t.UpdateBuyState()
}

func (t *Trade) DoAddSellFill(fill OrderFill) {
	t.State.SellSideFills = append(t.State.SellSideFills, fill)
	t.UpdateSellState()
}

func (t *Trade) AddClientOrderID(clientOrderID string) {
	t.State.ClientOrderIDs[clientOrderID] = true
}

func (t *Trade) UpdateSellState() {
	quantity := float64(0)
	totalPrice := float64(0)
	cost := float64(0)

	for _, fill := range t.State.SellSideFills {
		quantity += fill.Quantity
		totalPrice += fill.Price * fill.Quantity
		if fill.CommissionAsset == "BNB" {
			cost += (fill.Price * fill.Quantity) * (1 - BNB_FEE)
		} else {
			cost += (fill.Price * fill.Quantity) - fill.CommissionAmount
		}
	}

	if quantity > 0 {
		t.State.AverageSellPrice = round8(totalPrice / quantity)
		t.State.SellFillQuantity = round8(quantity)
		t.State.SellCost = round8(cost)
		t.State.Profit = t.State.SellCost - t.State.BuyCost
		t.State.ProfitPercent = t.State.Profit / t.State.BuyCost * 100
		t.State.Profit = round8(t.State.Profit)
		t.State.ProfitPercent = round8(t.State.ProfitPercent)
	}
}

func (t *Trade) UpdateBuyState() {
	var cost float64 = 0
	var totalPrice float64 = 0
	var quantity float64 = 0

	lastFee := float64(0)

	for _, fill := range t.State.BuySideFills {
		quantity += fill.Quantity
		totalPrice += fill.Price * fill.Quantity
		if fill.CommissionAsset == "BNB" {
			cost += (fill.Price * fill.Quantity) * (1 + BNB_FEE)
			lastFee = BNB_FEE
		} else {
			cost += (fill.Price * fill.Quantity)
			lastFee = DEFAULT_FEE
			quantity = quantity - fill.CommissionAmount
		}
	}

	if quantity > 0 {
		t.State.AverageBuyPrice = round8(totalPrice / quantity)
		t.State.BuyFillQuantity = round8(quantity)
		t.State.BuyCost = round8(cost)
		t.State.EffectiveBuyPrice = round8(cost / quantity)

		// Use the fee from the most recent fee as the effective fee used for
		// calculation in profit and losses.
		t.State.Fee = lastFee
	}
}

func round8(val float64) float64 {
	return math.Round(val*100000000) / 100000000
}
