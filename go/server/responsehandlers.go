// The MIT License (MIT)
//
// Copyright (c) 2018 Cranky Kernel
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy,
// modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
// BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
// ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

package server

import (
	"encoding/json"
	"fmt"
	"gitlab.com/crankykernel/maker/go/log"
	"net/http"
)

func WriteJsonResponse(w http.ResponseWriter, statusCode int, body interface{}) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(statusCode)
	encoder := json.NewEncoder(w)
	if body != nil {
		if err := encoder.Encode(body); err != nil {
			log.WithError(err).Errorf("Failed to write JSON response to client")
		}
	}
}

/// WriteJsonError writes a JSON formatted error response to the web client.
func WriteJsonError(w http.ResponseWriter, statusCode int, message string) {
	body := map[string]interface{}{
		"error":      true,
		"statusCode": statusCode,
	}
	if message != "" {
		body["message"] = message
	}
	WriteJsonResponse(w, statusCode, body)
}

func RequireFormValue(w http.ResponseWriter, r *http.Request, field string) bool {
	if r.FormValue(field) == "" {
		WriteJsonError(w, http.StatusBadRequest, fmt.Sprintf("%s is required", field))
		return false
	}
	return true
}

func WriteBadRequestError(w http.ResponseWriter) {
	w.WriteHeader(http.StatusBadRequest)
}
